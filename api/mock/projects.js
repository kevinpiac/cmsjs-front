export default {
  async getAllProjectByUserId(userId) {
    if (!userId) {
      throw 'Whatever';
    }
    return [
      {
        id: 'id1',
        name: 'Unnamed Project',
        websiteUrl: 'No Website URL',
        published: true,
      },
      {
        id: 'id2',
        name: 'Y Combinator',
        websiteUrl: 'http://news.ycombinator.com',
        published: true,
      },
      {
        id: 'id3',
        name: 'A A A Project',
        websiteUrl: 'http://a.com',
        published: false,
      },
      {
        id: 'id4',
        name: 'Crisp',
        websiteUrl: 'http://getcrispapp.co',
        published: false,
      },
      {
        id: 'id5',
        name: 'Asclepios',
        websiteUrl: 'http://associationasclepioscorte.com',
        published: true,
      },
    ];
  },
};
