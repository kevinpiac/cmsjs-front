import posts from './posts';
import settings from './settings';
import projects from './projects';

export default {
  posts, // DEMO DATA
  settings,
  projects,
};
