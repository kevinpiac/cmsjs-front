import Firebase from '~/plugins/firebase';

export default {
  async getSettingsByUserId(userId) {
    const snap = await Firebase.database()
      .ref('/settings')
      .orderByChild('userId')
      .equalTo(userId)
      .once('value');
    return snap.val();
  },
};
