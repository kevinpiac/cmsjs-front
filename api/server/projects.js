import Firebase from '~/plugins/firebase';
import { mapFirebaseObjectToArray } from '~/helpers';
import uuid from 'uuid/v1';

const updateProjectById = async (projectId, data) => {
  const snap = await Firebase.database()
    .ref('/projects')
    .child(projectId)
    .update(data);
  return data;
};

const updateProjectUrlById = async (projectId, url) => {
  return updateProjectById(projectId, { websiteUrl: url });
};

const isUrlReachable = async url => {
  // TODO integrate a real endpoint
  const a = new Promise(resolve => {
    if (url === 'http://notreachable') {
      setTimeout(() => resolve(false), 2000);
    } else {
      setTimeout(() => resolve(true), 2000);
    }
  });
  return Promise.resolve(a);
};

export default {
  updateProjectById,
  updateProjectUrlById,
  isUrlReachable,
  updateProjectById,
  async getAllProjectByUserId(userId) {
    const snap = await Firebase.database()
      .ref('/projects')
      .orderByChild('userId')
      .equalTo(userId)
      .once('value');
    return mapFirebaseObjectToArray(snap.val());
  },
  async getProjectById(projectId) {
    const snap = await Firebase.database()
      .ref('/projects')
      .child(projectId)
      .once('value');
    if (snap.val()) {
      return { ...snap.val(), id: projectId };
    }
    return null;
  },
  async createProject({ name, userId, websiteUrl, published = false } = {}) {
    const project = {
      name,
      userId,
      websiteUrl,
      published,
    };
    const snap = await Firebase.database()
      .ref('/projects')
      .push({
        name,
        userId,
        websiteUrl,
        published,
      });
    return { ...project, id: snap.key };
  },
};
