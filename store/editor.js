export const state = () => ({
  sessionId: null,
  editionMode: 'edition', // edition, preview
  selectedElementData: null,
  elementReplacements: {
    // { selector, changes: [{ replaceByHtml }] },
  },
  currentWebPageUrl: '',
});

export const getters = {
  currentWebPageUrl(state) {
    return state.currentWebPageUrl;
  },
  sessionId(state) {
    return state.sessionId;
  },
  selectedElementData(state) {
    return state.selectedElementData;
  },
  isModeEdition(state) {
    return state.editionMode === 'edition';
  },
  elementReplacements(state) {
    return state.elementReplacements;
  },
};

export const actions = {
  setCurrentWebPageUrl({ commit }, url) {
    commit('SET_CURRENT_PAGE_URL', url);
  },
  setSessionId({ commit }, sessionId) {
    commit('SET_SESSION_ID', sessionId);
  },
  setSelectedElementData({ commit }, selectedElementData) {
    commit('SET_SELECTED_ELEMENT_DATA', selectedElementData);
  },
  resetSelectedElementData({ commit }) {
    commit('SET_SELECTED_ELEMENT_DATA', null);
  },
  toggleEditionMode({ commit, dispatch, getters }) {
    if (getters.isModeEdition) {
      dispatch('postMessage', {
        name: 'setModeView',
      });
      dispatch('resetSelectedElementData');
      commit('SET_EDITION_MODE', 'preview');
    } else {
      dispatch('postMessage', {
        name: 'setModeEdition',
      });
      commit('SET_EDITION_MODE', 'edition');
    }
  },
  pushElementReplacement({ commit, getters }, { selector, replaceByHtml }) {
    commit('SET_ELEMENT_REPLACEMENT', {
      selectedElementData: getters.selectedElementData,
      replaceByHtml,
    });
  },
  postMessage({ getters }, { name, value = null, metadata } = {}) {
    const receiver = document.querySelector('.editable-frame').contentWindow;
    receiver.postMessage(
      {
        name,
        value,
        metadata,
        receiver: 'iframe',
        sender: 'editor',
      },
      getters.currentWebPageUrl,
    );
  },
};

export const mutations = {
  SET_CURRENT_PAGE_URL(state, url) {
    state.currentWebPageUrl = url;
  },
  SET_SESSION_ID(state, sessionId) {
    state.sessionId = sessionId;
  },
  SET_SELECTED_ELEMENT_DATA(state, data) {
    state.selectedElementData = null;
    state.selectedElementData = data;
  },
  SET_EDITION_MODE(state, mode) {
    state.editionMode = mode;
  },
  SET_ELEMENT_REPLACEMENT(state, { selectedElementData, replaceByHtml }) {
    // TODO: replace changes by specific changed value for each change:
    // { changes: [{ textAlign: left, color: blue }, { textAlign: center } ]}
    const selector = selectedElementData.selector;
    const cpy = state.elementReplacements;
    if (!cpy[selector]) {
      cpy[selector] = {
        selector,
        elementData: selectedElementData,
        lastChange: {
          // type: 'replaceByHtml',
          // value: ''
        },
        history: [],
        changesCount: 0,
      };
    }
    const change = {
      selector,
      type: 'replaceByHtml',
      value: replaceByHtml,
    };
    cpy[selector].lastChange = change;
    cpy[selector].history.push(change);
    cpy[selector].changesCount = cpy[selector].history.length;
    state.elementReplacements = null;
    state.elementReplacements = cpy;
  },
};
