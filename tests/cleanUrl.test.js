import test from 'ava';
import { cleanUrl } from '../helpers';

test('Should remove http://', t => {
  const result = cleanUrl('http://mydomainname.com');
  t.is('mydomainname.com', result);
});

test('Should remove https://', t => {
  const result = cleanUrl('https://mydomainname.com');
  t.is('mydomainname.com', result);
});

test('Should remove / at the end', t => {
  const result = cleanUrl('mydomainname.com/');
  t.is('mydomainname.com', result);
});

test('Should remove both / and https or http', t => {
  const result1 = cleanUrl('http://mydomainname.com/');
  const result2 = cleanUrl('https://mydomainname.com/');
  t.is('mydomainname.com', result1);
  t.is('mydomainname.com', result2);
});
