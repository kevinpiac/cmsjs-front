module.exports = {
  mainNav: {
    signUp: 'Sign Up',
    signIn: 'Sign In',
    logout: 'Logout',
  },
  signupPage: {
    title: 'Join us for free',
    basicSignUpAction: 'Sign Up Now',
  },
  signinPage: {
    title: 'We are glad to see you again!',
    basicSignInAction: 'Sign in',
  },
};
