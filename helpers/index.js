import jwtDecode from 'jwt-decode';
import _ from 'lodash';
const cookieparser = require('cookieparser');

export const getUserFromCookie = req => {
  if (!req.headers.cookie) return;

  if (req.headers.cookie) {
    const parsed = cookieparser.parse(req.headers.cookie);
    const accessTokenCookie = parsed.access_token;
    if (!accessTokenCookie) return;

    const decodedToken = jwtDecode(accessTokenCookie);
    if (!decodedToken) return;

    return decodedToken;
  }
};

export const mapFirebaseObjectToArray = obj => {
  if (!obj) {
    return [];
  }
  return Object.keys(obj).map(key => {
    const item = obj[key];
    return { ...item, id: key };
  });
};

export const cleanUrl = url => {
  let res = _.trimStart(url, 'https://');
  res = _.trimStart(res, 'http://');
  res = _.trimEnd(res, '/');
  return res;
};
